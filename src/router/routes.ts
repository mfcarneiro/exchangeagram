import { RouteRecordRaw } from 'vue-router';

const routes: RouteRecordRaw[] = [
	{ path: '/', redirect: '/home' },
	{
		path: '/',
		component: () => import('layouts/MainLayout.vue'),
		children: [
			{ path: 'home', component: () => import('pages/PageHome.vue') },
			{ path: 'camera', component: () => import('pages/PageCamera.vue') },
		],
	},
];

export default routes;
