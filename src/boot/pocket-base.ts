import { boot } from 'quasar/wrappers';
import PocketBase from 'pocketbase';

import { pocketBaseSymbol } from '../symbols/injection-symbols';

const client = new PocketBase(import.meta.env.VITE_POCKET_BASE_ENDPOINT);

export default boot(({ app }) => {
	app.provide(pocketBaseSymbol, client);
});
