/* eslint-env node */

/*
 * This file runs in a Node context (it's NOT transpiled by Babel), so use only
 * the ES6 features that are supported by your Node version. https://node.green/
 */

// Configuration for your app
// https://v2.quasar.dev/quasar-cli-vite/quasar-config-js

// const path = require('path');

const { configure } = require('quasar/wrappers');

module.exports = configure(function (/* ctx */) {
	return {
		eslint: {
			// fix: true,
			// include = [],
			// exclude = [],
			// rawOptions = {},
			warnings: true,
			errors: true,
		},

		// https://v2.quasar.dev/quasar-cli-vite/prefetch-feature
		// preFetch: true,

		// https://v2.quasar.dev/quasar-cli-vite/boot-files
		boot: ['axios', 'pocket-base', 'firebase-init', 'pwa-elements'],

		// https://v2.quasar.dev/quasar-cli-vite/quasar-config-js#css
		css: ['app.scss'],

		// https://github.com/quasarframework/quasar/tree/dev/extras
		extras: [
			'eva-icons',
			'mdi-v6',
			'roboto-font', // optional, you are not bound to it
		],

		// Full list of options: https://v2.quasar.dev/quasar-cli-vite/quasar-config-js#build
		build: {
			target: {
				browser: [
					'es2019',
					'edge88',
					'firefox78',
					'chrome87',
					'safari13.1',
				],
				node: 'node16',
			},

			vueRouterMode: 'history', // available values: 'hash', 'history'
			vueOptionsAPI: false,
			// vueRouterBase,
			// vueDevtools,

			// rebuildCache: true, // rebuilds Vite/linter/etc cache on startup

			// publicPath: '/',
			// analyze: true,
			// env: {},
			// rawDefine: {}
			// ignorePublicFolder: true,
			// minify: false,
			// polyfillModulePreload: true,
			// distDir

			// extendViteConf (viteConf) {},
			// viteVuePluginOptions: {},

			// vitePlugins: [
			//   [ 'package-name', { ..options.. } ]
			// ]
		},

		// Full list of options: https://v2.quasar.dev/quasar-cli-vite/quasar-config-js#devServer
		devServer: {
			// https: {
			// 	key: './localhost-key.pem',
			// 	cert: './localhost.pem',
			// },

			open: false, // opens browser window automatically
		},

		// https://v2.quasar.dev/quasar-cli-vite/quasar-config-js#framework
		framework: {
			iconSet: 'svg-eva-icons',
			config: {},

			// iconSet: 'material-icons', // Quasar icon set
			lang: 'en-US', // Quasar language pack

			// For special cases outside of where the auto-import strategy can have an impact
			// (like functional components as one of the examples),
			// you can manually specify Quasar components/directives to be available everywhere:
			//
			// components: [],
			// directives: [],

			// Quasar plugins
			plugins: ['Notify'],
		},

		// animations: 'all', // --- includes all animations
		// https://v2.quasar.dev/options/animations
		animations: ['fadeIn', 'fadeOut'],

		// https://v2.quasar.dev/quasar-cli-vite/quasar-config-js#sourcefiles
		sourceFiles: {
			// rootComponent: 'src/App.vue',
			// router: 'src/router/index',
			// store: 'src/store/index',
			// registerServiceWorker: 'src-pwa/register-service-worker',
			// serviceWorker: 'src-pwa/custom-service-worker',
			// pwaManifestFile: 'src-pwa/manifest.json',
			// electronMain: 'src-electron/electron-main',
			// electronPreload: 'src-electron/electron-preload'
		},

		// https://v2.quasar.dev/quasar-cli-vite/developing-pwa/configuring-pwa
		pwa: {
			workboxMode: 'injectManifest', // or 'injectManifest'
			swFilename: 'sw.js',
			manifestFilename: 'manifest.json',
			// injectPwaMetaTags: true,
			// useCredentialsForManifestTag: true,
			// useFilenameHashes: true,
			// extendGenerateSWOptions(cfg) {},
			// extendInjectManifestOptions(cfg) {},
			// extendManifestJson (json) {}
			// extendPWACustomSWConf (esbuildConf) {}
		},
	};
});
