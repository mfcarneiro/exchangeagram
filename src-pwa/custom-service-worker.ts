declare const self: ServiceWorkerGlobalScope & typeof globalThis;

import { clientsClaim } from 'workbox-core';
import { precacheAndRoute, cleanupOutdatedCaches } from 'workbox-precaching';
import { registerRoute } from 'workbox-routing';
import { CacheFirst, StaleWhileRevalidate } from 'workbox-strategies';
import { ExpirationPlugin } from 'workbox-expiration';
import { CacheableResponsePlugin } from 'workbox-cacheable-response';
import { Queue } from 'workbox-background-sync';

self.skipWaiting();

clientsClaim();

precacheAndRoute(self.__WB_MANIFEST);

cleanupOutdatedCaches();

// Cache on top to access before the http
registerRoute(
	({ url }) => url.host.startsWith('fonts.g'),
	new CacheFirst({
		cacheName: 'google-fonts',
		plugins: [
			new ExpirationPlugin({
				maxEntries: 60,
				maxAgeSeconds: 30 * 24 * 60 * 60, // 30 Days
			}),
			new CacheableResponsePlugin({
				statuses: [0, 200],
			}),
		],
	})
);

registerRoute(
	({ url }) => url.href.startsWith('http'),
	new StaleWhileRevalidate({
		plugins: [
			new CacheableResponsePlugin({
				statuses: [0, 200],
			}),
		],
	})
);

let createPostQueue: Queue;

if ('sync' in self.registration) {
	createPostQueue = new Queue('createPostQueue');

	self.addEventListener('fetch', (event) => {
		if (event.request.method !== 'POST') return;

		if (
			event.request.url.endsWith('/posts/records') ||
			event.request.url.match('user-posts%')
		) {
			async function createPostSync(): Promise<Response> {
				try {
					return await fetch(event.request.clone());
				} catch (error) {
					await createPostQueue.pushRequest({ request: event.request });

					return Response.error();
				}
			}

			event.respondWith(createPostSync());
		}
	});
}
