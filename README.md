# <div align="center"><p align="center">Exchangeagram</p></div>

Share your photos \"on-the-line\"

_The App will remain as simple as possible. An entry point to receive PWA features._

_Later the proper separation of concerns and architecture will be added._

## Install the dependencies

```bash
yarn
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)

```bash
quasar dev
# or
quasar d -m pwa
```

### Lint the files

```bash
yarn lint
```

### Format the files

```bash
yarn format
```

### Build the app for production

```bash
quasar build -m pwa
```
